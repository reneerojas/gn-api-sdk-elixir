defmodule GnApiSdkElixir.MixProject do
  use Mix.Project

  def project do
    [
      app: :gerencianet,
      version: "0.1.10",
      elixir: "~> 1.10",
      start_permanent: Mix.env() == :prod,
      description: description(),
      package: package(),
      deps: deps(),
      docs: docs(),
      name: "Gerencianet",
      source_url: "https://gitlab.com/reneerojas/gn-api-sdk-elixir"
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:ex_doc, "~> 0.25.5"},
      {:jason, "~> 1.1"},
      {:httpoison, "~> 2.1"}
    ]
  end

  defp description() do
    "SDK de integração Gerencianet. Detalhes em https://dev.gerencianet.com.br"
  end

  defp package() do
    [
      # This option is only needed when you don't want to use the OTP application name
      name: "gerencianet",
      # These are the default files included in the package
      files: ~w(lib mix.exs .formatter.exs README*),
      licenses: ["Apache-2.0"],
      links: %{"GitLab" => "https://gitlab.com/reneerojas/gn-api-sdk-elixir"}
    ]
  end

  defp docs() do
    [
      main: "configuracao",
      formatter_opts: [gfm: true],
      source_ref: @version,
      source_url: "https://gitlab.com/reneerojas/gn-api-sdk-elixir",
      extras: [
        "docs/Configuracao.md"]
    ]
  end
end
