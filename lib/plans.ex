defmodule GnApiSdkElixir.Plans do
  @moduledoc """
  Módulo de integração de planos
  """

  alias GnApiSdkElixir.Request

  @doc """
  Busca informações relacionadas à uma assinatura.
  """
  def list do
    Request.get("/v1/plans")
  end

  @doc """
  Cria o plano de assinatura, sendo definido pelo integrador o nome do plano, intervalo (em meses) em que a assinatura será gerada e a quantidade de repetições que a cobrança será gerada.
  """
  @type plan :: %{name: String.t, interval: integer, repeats: integer}
  def create(plan) do
    Request.post("/v1/plan", plan)
  end

  @doc """
  Permite alterar (editar) o nome de um plano de assinatura pré existente. Para tal, deve ser fornecido o identificador do plan_id desejado.
  """
  def update(id, %{name: name}) do
    Request.put("/v1/plan/#{id}", %{name: name})
  end

  @doc """
  Permite cancelar um plano de assinatura. Para tal, você deve informar o plan_id.
  """
  def delete(id) do
    Request.delete("/v1/plan/#{id}")
  end
end
