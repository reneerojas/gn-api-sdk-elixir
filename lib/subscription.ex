defmodule GnApiSdkElixir.Subscription do
  @moduledoc """
  Módulo de integração de assinaturas
  """

  alias GnApiSdkElixir.Request

  @doc """
  Cria uma assinatura quando você precisa cobrar de forma recorrente seus clientes.
  esta forma, os custos subsequentes serão criados automaticamente com base na Requesturação do plano.
  Para tal, você deve informar o plan_id do plano criado previamente no qual deseja associar a uma assinatura.
  """
  @type items :: [%{name: String.t, value: integer, amount: integer}]
  @type metadata :: %{custom_id: String.t | nil, notification_url: String.t | nil} | nil
  def create(plan_id, items, metadata) do
    Request.post("/v1/plan/#{plan_id}/subscription", %{items: items, metadata: metadata})
  end

  @doc """
  Possibilita buscar informações de uma assinatura que está vinculada a um plano.
  """
  def get(subscription_id) do
    Request.get("/v1/subscription/#{subscription_id}")
  end

  @doc """
  Permite cancelar assinaturas ativas em um plano de assinaturas.
  Para tal, deve-se informar o subscription_id da assinatura que deseja cancelar.
  """
  def cancel(subscription_id) do
    Request.put("/v1/subscription/#{subscription_id}/cancel", %{})
  end

  @doc """
  Altera as informações enviadas na propriedade metadata de uma assinatura a qualquer momento.
  Este endpoint é de extrema importância para atualizar sua URL de notificação atrelada às assinaturas ou modificar o custom_id previamente associado a assinatura.
  """
  def metadata(subscription_id, metadata) do
    Request.put("/v1/subscription/#{subscription_id}/metadata", metadata)
  end

  @doc """
  Define uma forma recorrente de pagamento à uma determinada assinatura.
  Ela poderá ser por cartão de crédito ou boleto bancário
  """
  def pay(subscription_id, payment) do
    Request.post("/v1/subscription/#{subscription_id}/pay", payment)
  end

  @doc """
  O histórico de uma assinatura representa todas as ações que ocorreram com esta assinatura até o presente momento.
  É possível adicionar mensagens personalizadas a este histórico
  """
  def history(subscription_id, description) do
    Request.post("/v1/subscription/#{subscription_id}/history", description)
  end
end
