defmodule GnApiSdkElixir.Charge do
  @moduledoc """
  Módulo de integração de cobranças
  """

  alias GnApiSdkElixir.Request

  @doc """
  Permite criar uma transação já associando um método de pagamento, podendo ser boleto bancário ou cartão de crédito, em apenas uma etapa.
  """
  def create_one_step(items, payment, metadata \\ nil, shippings \\ []) do
    charge = %{items: items, payment: payment}
             |> then(&(if !is_nil(metadata), do: Map.merge(&1, %{metadata: metadata}), else: &1))
             |> then(&(if !Enum.empty?(shippings), do: Map.merge(&1, %{shippings: shippings}), else: &1))
    Request.post("/v1/charge/one-step", charge)
  end

  @doc """
  Permite criar uma nova transação; retorna um código identificador da transação denominado charge_id.
  """
  def create(items) do
    Request.post("/v1/charge", %{items: items})
  end

  @doc """
  Permite criar uma nova transação; retorna um código identificador da transação denominado charge_id.
  """
  def get(charge_id) do
    Request.get("/v1/charge/#{charge_id}")
  end

  @doc """
  Permite incluir informações como notification_url e custom_id à uma transação existente.
  Este endpoint é de extrema importância para atualizar sua URL de notificação atrelada às transações ou modificar o custom_id previamente associado às suas transações.
  """
  @type metadata :: %{custom_id: String.t, notification_url: String.t}
  def metadata(charge_id, metadata) do
    Request.put("/v1/charge/#{charge_id}/metadata", metadata)
  end

  @doc """
  Permite efetuar a alteração da data de vencimento de uma transação em que a forma de pagamento é boleto bancário (banking_billet) e que ainda não foi paga.
  O formato da data de vencimento deve seguir o seguinte padrão: YYYY-MM-DD.
  """
  def billet(charge_id, expire) do
    Request.put("/v1/charge/#{charge_id}/billet", %{expire_at: expire})
  end

  @doc """
  Através deste endpoint é possível cancelar uma transação criada.
  Somente transações com status new, waiting, unpaid ou link podem ser canceladas.
  """
  def cancel(charge_id) do
    Request.put("/v1/charge/#{charge_id}/cancel", %{})
  end

  @doc """
  Permite associar um método de pagamento à uma transação já criada.
  Após gerar uma transação através do endpoint POST /v1/charge, esta fica classificada com o status de new (novo), ou seja, uma nova transação foi gerada, porém, nenhum método de pagamento foi atribuído a ela.
  """
  def pay(charge_id, payment) do
    Request.post("/v1/charge/#{charge_id}/pay", %{payment: payment})
  end

  @doc """
  Permite o reenvio do boleto bancário para o e-mail desejado.
  """
  def billet_resend(charge_id, email) do
    Request.post("/v1/charge/#{charge_id}/billet/resend", %{email: email})
  end

  @doc """
  O histórico de uma transação representa todas as ações que ocorreram com esta transação até o presente momento.
  É possível adicionar mensagens personalizadas a este histórico utilizando o endpoint /v1/charge/:id/history.
  """
  def history(charge_id, description) do
    Request.post("/v1/charge/#{charge_id}/history", description)
  end

  @doc """
  Permite retornar um link para uma tela de pagamento da Gerencianet.
  Em outras palavras, o integrador gera uma cobrança e, em seguida, ao invés de definir o pagamento via boleto bancário ou cartão de crédito, o integrador pode solicitar um link escolhendo inclusive se a tela de pagamento deve aceitar boleto, cartão ou ambos..
  """
  def link(charge_id, %{message: message, expire_at: expire_at, request_delivery_address: request_delivery_address, payment_method: payment_method}) do
    Request.post("/v1/charge/#{charge_id}/link", %{message: message, expire_at: expire_at, request_delivery_address: request_delivery_address, payment_method: payment_method})
  end

  @doc """
  Permite retornar um link para uma tela de pagamento da Gerencianet.
  Em outras palavras, o integrador gera uma cobrança e, em seguida, ao invés de definir o pagamento via boleto bancário ou cartão de crédito, o integrador pode solicitar um link escolhendo inclusive se a tela de pagamento deve aceitar boleto, cartão ou ambos..
  """
  def link_update(charge_id, expire_at) do
    Request.put("/v1/charge/#{charge_id}/link", %{expire_at: expire_at})
  end

  @doc """
  Você pode definir que a transação será do tipo boleto balancete. Este é um modelo muito utilizado por condomínios e contabilidades.
  """
  def balance_sheet(charge_id, data) do
    Request.post("/v1/charge/#{charge_id}/balance-sheet", data)
  end

  @doc """
  Você pode definir que a transação será do tipo boleto balancete. Este é um modelo muito utilizado por condomínios e contabilidades.
  """
  def settle(charge_id) do
    Request.put("/v1/charge/#{charge_id}/settle", %{})
  end
end
