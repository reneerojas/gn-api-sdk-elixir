defmodule GnApiSdkElixir.Carnet do
  @moduledoc """
  Um carnê é um conjunto de transações (parcelas) geradas em lote e com forma de pagamento já definida.
  As parcelas de um carnê vencem mensalmente, de acordo com a data definida pelo integrador.
  """

  alias GnApiSdkElixir.Request

  @doc """
  Utilizado para geração de um conjunto de transações (parcelas) geradas em lote e com uma forma de pagamento já definida.
  As parcelas de um carnê vencem mensalmente, de acordo com a data definida pelo integrador.
  """
  def create(data) do
    Request.post("/v1/carnet", data)
  end

  @doc """
  Retorna informações sobre um carnê criado. Cada transação criada via carnê possui uma única chave identificadora que a identifica.
  """
  def get(carnet_id) do
    Request.get("/v1/carnet/#{carnet_id}")
  end

  @doc """
  Permite incluir em um carnê informações como notification_url e custom_id.
  Para isso, você precisa informar o carnet_id do carnê existente no qual deseja incluir essas informações.
  Este endpoint é de extrema importância para atualizar sua URL de notificação atrelada ao carnê ou modificar o custom_id previamente associado aos carnês.
  """
  def metadata(carnet_id, metadata) do
    Request.put("/v1/carnet/#{carnet_id}", metadata)
  end

  @doc """
  Possibilita alterar a data de vencimento de uma determinada parcela de um carnê.
  Para tal, é necessário que você informe o carnet_id, a parcela que deseja e a expire_at (nova data de vencimento, no formato YYYY-MM-DD).
  """
  def parcel(carnet_id, parcel, expire_at) do
    Request.put("/v1/carnet/#{carnet_id}/parcel/#{parcel}", %{expire_at: expire_at})
  end

  @doc """
  Possibilita alterar a data de vencimento de uma determinada parcela de um carnê.
  Para tal, é necessário que você informe o carnet_id, a parcela que deseja e a expire_at (nova data de vencimento, no formato YYYY-MM-DD).
  """
  def cancel(carnet_id) do
    Request.put("/v1/carnet/#{carnet_id}/cancel", %{})
  end

  @doc """
  Possibilita efetuar o cancelamento de uma parcela específica de um carnê existente.
  Para isso, você deve informar o carnet_id e o número da parcela que deseja efetuar o cancelamento.
  """
  def parcel_cancel(carnet_id, parcel) do
    Request.put("/v1/carnet/#{carnet_id}/parcel/#{parcel}/cancel", %{})
  end

  @doc """
  Este método possibilita que o carnê seja reenviado para um endereço de email.
  É preciso que você informe o carnet_id do carnê desejado.
  """
  def resend(carnet_id, email) do
    Request.post("/v1/carnet/#{carnet_id}/resend", %{email: email})
  end

  @doc """
  Este método possibilita que uma determinada parcela de um carnê seja reenviado para um endereço de e-mail.
  Para tal, você deve informar o carnet_id desejado.
  """
  def parcel_resend(carnet_id, parcel, email) do
    Request.post("/v1/carnet/#{carnet_id}/parcel/#{parcel}/resend", %{email: email})
  end

  @doc """
  O histórico de um carnê é semelhante ao histórico de uma transação. Porém, ele reflete as ações que o carnê em si sofreu.
  E, da mesma forma, é possível adicionar mensagens personalizadas ao histórico de um carnê, sem que estas, contudo, influenciem no fluxo do mesmo.
  """
  def history(carnet_id, description) do
    Request.post("/v1/carnet/#{carnet_id}/history", description)
  end

  @doc """
  Permite marcar como pago (baixa manual) uma determinada parcela de um carnê.
  Para tal, você deverá informar o carnet_id e o número da parcela do carnê que deseja marcar como pago.
  """
  def settle(carnet_id, parcel) do
    Request.put("/v1/carnet/#{carnet_id}/parcel/#{parcel}/settle", %{})
  end
end
