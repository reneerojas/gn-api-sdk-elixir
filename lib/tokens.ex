defmodule GnApiSdkElixir.Tokens do
  @moduledoc """
  Utilizado para gera tokens de pagamento e recuperar dados para criptografia
  """

  alias GnApiSdkElixir.Request

  def opt do
    [timeout: 50_000, recv_timeout: 50_000]
  end

  @doc """
  Salt para criptografia de cartao
  """
  def salt() do
    with {:ok, resp} <- HTTPoison.get("https://tokenizer.gerencianet.com.br/salt", [{"account-code", Application.get_env(:gerencianet, :payee_code)}], opt())
                        |> Request.debug()
                        |> GnApiSdkElixir.Request.parse_response() do
      resp.data
    end
  end

  @doc """
  Recupera a chave publica de criptografia
  """
  def public_key() do
    with {:ok, resp} <- HTTPoison.get("#{Request.base_url()}/v1/pubkey?code=#{Application.get_env(:gerencianet, :payee_code)}", opt())
                        |> Request.debug()
                        |> GnApiSdkElixir.Request.parse_response() do
      resp.data
    end
  end

  @doc """
  Gera o payment_token para os dados do cartao o cartao
  """
  def card(card) do
    headers = [{"account-code", Application.get_env(:gerencianet, :payee_code)},{"Content-Type", "application/json"}]
    url = if (Application.get_env(:gerencianet, :sandbox)), do: "#{Request.base_url()}/v1/card",
                                                            else: "https://tokenizer.gerencianet.com.br/card"
    with {:ok, resp} <- HTTPoison.post(url, Jason.encode!(%{data: card}), headers, opt())
                        |> Request.debug()
                        |> GnApiSdkElixir.Request.parse_response() do
      resp.data
    end
  end
end
