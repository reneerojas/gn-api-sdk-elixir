defmodule GnApiSdkElixir.Notification do
  @moduledoc """
    As notificações permitem que você seja informado quando uma transação (também chamada de "cobrança") tiver seu status alterado. Dessa forma, você poderá identificar quando um boleto for pago, por exemplo.
  """

  alias GnApiSdkElixir.Request

  @doc """
  Retorna o histórico de notificações enviadas a uma determinada transação
  """
  def get(token) do
    Request.get("/v1/notification/#{token}")
  end
end
