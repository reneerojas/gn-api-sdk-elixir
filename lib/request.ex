defmodule GnApiSdkElixir.Request do
  @moduledoc false

  def base_url do
    if Application.get_env(:gerencianet, :sandbox),
       do: "https://sandbox.gerencianet.com.br",
       else: "https://api.gerencianet.com.br"
  end

  def headers do
    [{"Content-Type", "application/json"}]
  end

  def opt do
    [timeout: 50_000, recv_timeout: 50_000]
  end

  def auth do
    body = Jason.encode!(%{grant_type: :client_credentials})

    credentials = "#{Application.get_env(:gerencianet, :client_id)}:#{Application.get_env(:gerencianet, :client_secret)}"
                  |> Base.encode64()

    headers = [{"Authorization", "Basic #{credentials}"} | headers()]

    HTTPoison.post("#{base_url()}/v1/authorize", body, headers)
    |> parse_response()
  end

  def access_header do
    with {:ok, tokens} <- auth() do
      {:ok, {"Authorization", "Bearer #{tokens.access_token}"}}
    end
  end

  def get(uri) do
    with {:ok, token} <- access_header() do
      HTTPoison.get("#{base_url()}#{uri}", [token | headers()], opt())
      |> debug()
      |> parse_response()
    end
  end

  def post(uri, data) do
    with {:ok, token} <- access_header() do
      HTTPoison.post("#{base_url()}#{uri}", Jason.encode!(data), [token | headers()], opt())
      |> debug()
      |> parse_response()
    end
  end

  def put(uri, data) do
    with {:ok, token} <- access_header() do
      HTTPoison.put("#{base_url()}#{uri}", Jason.encode!(data), [token | headers()], opt())
      |> debug()
      |> parse_response()
    end
  end

  def delete(uri) do
    with {:ok, token} <- access_header() do
      HTTPoison.delete("#{base_url()}#{uri}", [token | headers()], opt())
      |> debug()
      |> parse_response()
    end
  end

  def debug(request) do
    if Application.get_env(:gerencianet, :debug) do
      IO.inspect(request)
    end
    request
  end

  def parse_response(request) do
    case request do
      {:ok, %HTTPoison.Response{status_code: 200, body: body}} ->
        {:ok, Jason.decode!(body, keys: :atoms)}
      {:ok, %HTTPoison.Response{status_code: 500, body: body}} ->
        {:error, Jason.decode!(body, keys: :atoms)}
      {:ok, %HTTPoison.Response{status_code: 400, body: body}} ->
        {:error, Jason.decode!(body, keys: :atoms)}
      {:ok, %HTTPoison.Response{status_code: 404}} ->
        {:error, 404}
      {:error, %HTTPoison.Error{reason: reason}} ->
        {:error, reason}
    end
  end

end
