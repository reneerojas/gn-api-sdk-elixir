# GnApiSdkElixir

Esta documentação é destinada a desenvolvedores que desejam integrar seu site ou aplicação com a API da Gerencianet. São descritas as funcionalidades da API, módulos, parâmetros técnicos, vídeos em formato de aulas sobre integrações com nossa API e disponibilização de códigos prontos de exemplos para facilitar suas atividades de desenvolvimento.

A Gerencianet é uma Instituição de Pagamentos (IP) autorizada pelo Banco Central do Brasil. Com nossas soluções, você pode emitir cobranças para seus clientes - boleto, carnê, links de pagamento e assinaturas - e realizar movimentações financeiras em sua Conta Digital Gerencianet.

## Installation

If [available in Hex](https://hex.pm/docs/publish), the package can be installed
by adding `gerencianet` to your list of dependencies in `mix.exs`:

```elixir
def deps do
  [
    {:gerencianet, "~> 0.1.7"}
  ]
end
```

Configure seus ambientes dev.exs/prod.exs
```elixir
config :gerencianet,
client_id: "Client_Id",
client_secret: "Client_Secret",
sandbox: true
```

Documentation can be generated with [ExDoc](https://github.com/elixir-lang/ex_doc)
and published on [HexDocs](https://hexdocs.pm). Docs can
be found at [https://hexdocs.pm/gerencianet/](https://hexdocs.pm/gerencianet/).

